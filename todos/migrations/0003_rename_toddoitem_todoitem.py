# Generated by Django 4.0.5 on 2022-06-14 19:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0002_toddoitem'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ToddoItem',
            new_name='TodoItem',
        ),
    ]
