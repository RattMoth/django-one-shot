from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy

from todos.models import TodoItem, TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "lists/list.html"
    context_object_name = "todo_lists"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "lists/detail.html"
    context_object_name = "list"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "lists/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "lists/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "lists/delete.html"

    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])
